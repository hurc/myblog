---
layout: post
title:  "搭建局域网文件服务器"
tags: 工具 系统配置
mathjax: on
---
__由于家里网速太慢，还有很多时候手头的设备之间不太方便用互联网分享，所以我需要一款在局域网实现文件共享的工具，在众多工具的使用中最后倾向于在linux端使用apache做自己的文件服务器，安卓端使用x-plore__
###  linux端（deepin）
- apache：
  安装命令：sudo apt install apache2
  安装成功后进行配置：

  ```
  sudo vim /etc/apache2/sites-available/000-default.conf
  ```

  找到 DocumentRoot /var/www/html

  把他改为自己想要分享的文件夹就行了

  随后启动apache2

  ```
  sudo /etc/init.d/apache2 start #启动apache2
  ```

  更多apache2命令：

  ```
  sudo /etc/init.d/apache2 stop #关闭apache2
  sudo /etc/init.d/apache2 restart #重启apache2
  ```

  

### x-plore (android/安卓端)

下载x-plore

打开无线网络共享<img src="http://hurc.gitee.io/pic/_pic/Screenshot_2020-11-01-15-26-35-25.png" alt="jiepin" style="zoom: 25%;" />



其他设备在浏览器输入图中所示网址即可使用

