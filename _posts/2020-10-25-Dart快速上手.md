---
layout: post
title: 'Dart快速上手'
tags: flutter 编程
---

### 快速入门速成笔记

这是一个最基本的输出

```dart
main(List<String> args) {
  print('hello_world');
}
```

**dart 中使用可变变量**(个人理解)为${data}  如果表达式是一个**非拼接标识符**, 那么{}可以省略

```dart
main(List<String> args) {
  String ni = 'hello';
  print('$ni' + '_world');
  print('${ni}_world');
}
```

**类型推导声明变量** 

```dart
 var/dynamic/const/final // 变量名称 = 赋值 dynamic一般不用,有危险
```

**var的正常使用**

```dart
main(List<String> args) {
  var name = 'hurc';
  name = 'abaohu';
  print(name.runtimeType);//runtimeType获取当前变量
  print('$name');
}
//输出结果:
//String
//abaohu
```

**var的错误使用**

```dart
var age = 18;
age = 'hurc'; // 不可以将String赋值给一个int类型
//输出结果是报错
```

**dynamic**可以这样改变变量类型(**所以一般不会使用**)

**final&&const** 

**相同点:** 都是定义后值不能修改(定义变量)

```dart
// final错误示范
main(List<String> args) {
  final age = 18;
  age = 20;
}
// 输出error
```

```dart
// const错误示范
main(List<String> args) {
  const name = 'hurc';
  name = 'abaohu'
}
//输出error
```

**不同点:**  const必须是编译前获取值，而final则可以动态获取


```
String getName() {
  return 'coderhurc';
}

main(List<String> args) {
  final name = getName(); //done
  const name = getName(); //error
}
```

**数字类型**

**整数int** 小数double

**bool类型**：ture&&false

注意：dart不能判断非空即真，非零即真

**集合类型list/set/map**

**list 使用[]**

**set 使用{}**

两者区别set无序不重复

**map使用{}**

```dart
main(List<String> args) {
  var name = ['hurc', 'rchu', 'hurc'];
  var source = {'yuwne', 'shuxue'};
  var info = {'name': 'hurc', 'age': 18};
}
//三种用法
```

