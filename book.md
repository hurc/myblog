---
layout: page
title: Book
permalink: /book
---

##  这是我的长篇笔记系列
###  某些系统性比较强的我将采用gitbook这样比较有结构性
- 生物笔记: <http://hurc.gitee.io/book/shengwu>
- 地理笔记: <http://hurc.gitee.io/book/dili>
- 物理笔记: <http://hurc.gitee.io/book/wuli>
- 语文笔记: <http://hurc.gitee.io/book/yuwen>
- 日语笔记: <http://hurc.gitee.io/book/riyu>
- 数学笔记: <http://hurc.gitee.io/book/shuxue>

